import chai from 'chai';
import chaiHttp from 'chai-http';
import UserModel from '../src/modules/user/user.entity';
import UserLikeModel from '../src/modules/userLike/userLike.entity';
import app from '../src/server';

const generateUser = (username: string) => ({
  username,
  password: 'Test123!',
});

const user1 = { loginData: generateUser('user1'), _id: '' };
const user2 = { loginData: generateUser('user2'), _id: '' };
const user3 = { loginData: generateUser('user3'), _id: '' };

chai.use(chaiHttp);

let server: any;
describe('API Tests', async () => {
  before(async () => {
    server = await app;
    await UserLikeModel.remove({});
    await UserModel.remove({});
  });

  [user1, user2, user3].map((user) =>
    it(`Should create user: ${user.loginData.username}`, async () => {
      const response = await chai.request(server).post('/signup').send(user.loginData);

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body.data.username).to.equal(user.loginData.username);
      chai.expect(response.body.data.password).not.exist;

      switch (response.body.data.username) {
        case user1.loginData.username:
          user1._id = response.body.data._id;
          break;
        case user2.loginData.username:
          user2._id = response.body.data._id;
          break;
        case user3.loginData.username:
          user3._id = response.body.data._id;
          break;
      }
    })
  );

  it(`Should login user: ${user1.loginData.username}`, async () => {
    const response = await chai.request(server).post('/login').send(user1.loginData);

    chai.expect(response.status).to.equal(200);
    chai.expect(response.body.data.token).to.be.a('string');
  });

  it(`Should get user by token having user info without sensitive data of a user: ${user1.loginData.username}`, async () => {
    const responseLogin = await chai.request(server).post('/login').send(user1.loginData);

    const response = await chai.request(server).get('/me').set('Authorization', `Bearer ${responseLogin.body.data.token}`).send();
    chai.expect(response.status).to.equal(200);
    chai.expect(response.body.data.username).to.equal(user1.loginData.username);
    chai.expect(response.body.data.password).not.exist;
  });

  it(`Should update password of a user: ${user3.loginData.username}`, async () => {
    const responseLogin = await chai.request(server).post('/login').send(user3.loginData);

    const response = await chai
      .request(server)
      .put('/me/update-password')
      .set('Authorization', `Bearer ${responseLogin.body.data.token}`)
      .send({
        oldPassword: user3.loginData.password,
        newPassword: `${user3.loginData.password}1`,
      });
    chai.expect(response.status).to.equal(200);
    chai.expect(response.body.data.username).to.equal(user3.loginData.username);
    chai.expect(response.body.data.password).not.exist;

    const responseLogin2 = await chai
      .request(server)
      .post('/login')
      .send({ ...user3.loginData, password: `${user3.loginData.password}1` });

    chai.expect(responseLogin2.status).to.equal(200);
    chai.expect(responseLogin2.body.data.token).to.be.a('string');
  });

  it('Like, get user likes, unlike', async () => {
    const responseLogin = await chai.request(server).post('/login').send(user1.loginData);

    const responseLike = await chai.request(server).put(`/user/${user2._id}/like`).set('Authorization', `Bearer ${responseLogin.body.data.token}`);
    chai.expect(responseLike.status).to.equal(200);
    chai.expect(responseLike.body.data.likedBy.length).to.equal(1);

    const responseGetLikes = await chai.request(server).get(`/user/${user2._id}`);
    chai.expect(responseGetLikes.status).to.equal(200);
    chai.expect(responseGetLikes.body.data.user.username).to.equal(user2.loginData.username);

    const responseUnike = await chai.request(server).put(`/user/${user2._id}/unlike`).set('Authorization', `Bearer ${responseLogin.body.data.token}`);
    chai.expect(responseUnike.status).to.equal(200);
    chai.expect(responseUnike.body.data.likedBy.length).to.equal(0);
  });

  it('All users with likes', async () => {
    const responseLogin1 = await chai.request(server).post('/login').send(user1.loginData);
    const responseLogin2 = await chai.request(server).post('/login').send(user2.loginData);

    const responseLike1 = await chai.request(server).put(`/user/${user2._id}/like`).set('Authorization', `Bearer ${responseLogin1.body.data.token}`);
    chai.expect(responseLike1.status).to.equal(200);

    const mostLiked1 = await chai.request(server).get('/most-liked');

    chai.expect(mostLiked1.status).to.equal(200);
    chai.expect(mostLiked1.body.data.length).to.equal(3);
    chai.expect(mostLiked1.body.data[0].user._id).to.equal(user2._id);
    chai.expect(mostLiked1.body.data[0].numberOfLikes).to.equal(1);
    chai.expect(mostLiked1.body.data[1].numberOfLikes).to.equal(0);
    chai.expect(mostLiked1.body.data[2].numberOfLikes).to.equal(0);

    let responseLike2 = await chai.request(server).put(`/user/${user3._id}/like`).set('Authorization', `Bearer ${responseLogin1.body.data.token}`);
    responseLike2 = await chai.request(server).put(`/user/${user3._id}/like`).set('Authorization', `Bearer ${responseLogin2.body.data.token}`);

    const mostLiked2 = await chai.request(server).get('/most-liked');

    chai.expect(mostLiked2.status).to.equal(200);
    chai.expect(mostLiked2.body.data.length).to.equal(3);
    chai.expect(mostLiked2.body.data[0].user._id).to.equal(user3._id);
    chai.expect(mostLiked2.body.data[0].numberOfLikes).to.equal(2);
    chai.expect(mostLiked2.body.data[1].user._id).to.equal(user2._id);
    chai.expect(mostLiked2.body.data[1].numberOfLikes).to.equal(1);
    chai.expect(mostLiked2.body.data[2].numberOfLikes).to.equal(0);
  });
});
