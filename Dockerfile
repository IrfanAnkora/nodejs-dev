FROM node:10

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./

COPY tsconfig.json ./

COPY src ./src

RUN npm install

RUN npm run build

RUN rm -rf ./src

EXPOSE 4000

CMD ["node", "./dist/server.js"]