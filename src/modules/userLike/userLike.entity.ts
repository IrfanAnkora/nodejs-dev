import { prop, getModelForClass, Ref, arrayProp, mongoose } from '@typegoose/typegoose';
import { WhatIsIt } from '@typegoose/typegoose/lib/internal/constants';
import { User } from '../user/user.entity';

export class UserLike {
  @prop({
    ref: User,
    refType: mongoose.Schema.Types.ObjectId,
  })
  public user?: Ref<User | string>;

  @prop(
    {
      ref: User,
      refType: mongoose.Schema.Types.ObjectId,
    },
    WhatIsIt.ARRAY
  )
  public likedBy!: Ref<User>[];

  constructor(obj?: UserLike) {
    Object.assign(this, obj);
  }
}

const UserLikeModel = getModelForClass(UserLike);

export default UserLikeModel;
