import { Response, Router } from 'express';
import { authUser } from '../../core/auth.middleware';
import { RequestType } from '../../core/request.type';
import { generateFailure, generateResponse } from '../../core/response';
import userLikeService from './userLike.service';

class UserLikeController {
  constructor(router: Router) {
    router.route('/user/:id/like').put(authUser, this.like);
    router.route('/user/:id/unlike').put(authUser, this.unlike);
    router.route('/user/:id').get(this.totalUserLikes);
    router.route('/most-liked').get(this.mostLikedUsers);
  }

  totalUserLikes = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await userLikeService.count(req.params.id));
    } catch (e) {
      return generateFailure(res, e);
    }
  };

  like = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await userLikeService.like(req.user, req.params.id));
    } catch (e) {
      return generateFailure(res, e);
    }
  };

  unlike = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await userLikeService.unlike(req.user, req.params.id));
    } catch (e) {
      return generateFailure(res, e);
    }
  };

  mostLikedUsers = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await userLikeService.mostLikedUsers());
    } catch (e) {
      return generateFailure(res, e);
    }
  };
}

export default UserLikeController;
