import { mongoose } from '@typegoose/typegoose';
import { throwError } from '../../core/response';
import { UserDto } from '../user/user.dto';
import UserModel, { User } from '../user/user.entity';
import UserLikeModel, { UserLike } from './userLike.entity';

class UserLikeService {
  count = async (userId: string): Promise<UserLike> => {
    const userLikedArray = await UserLikeModel.aggregate([
      {
        $match: { user: new mongoose.Types.ObjectId(`${userId}`) },
      },
      {
        $addFields: {
          numberOfLikes: {
            $size: '$likedBy',
          },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $unwind: '$user',
      },
    ]);

    if (userLikedArray.length) {
      const userLiked = userLikedArray[0];
      return {
        ...userLiked,
        user: new UserDto(userLiked.user),
      };
    }

    throwError('UserNotFound', { id: userId }, 404);
  };

  like = async (user: User, userId: string): Promise<UserLike> => {
    const userLikes = new UserDto(user);

    const userLike = await UserLikeModel.findOne({ user: userId });

    if (!userLike) {
      throwError('UserNotFound', { id: userId }, 404);
    }

    const likeExist = userLike.likedBy.includes(user._id);
    if (likeExist) {
      return userLike;
    }

    const liked = await UserLikeModel.findOneAndUpdate(
      { user: userId },
      {
        $push: { likedBy: userLikes._id },
      },
      {
        new: true,
      }
    );

    return liked.toJSON();
  };

  unlike = async (user: User, userId: string): Promise<UserLike> => {
    const userLikes = new UserDto(user);

    const userExists = await UserModel.findOne({ _id: userId });
    if (!userExists) {
      throwError('UserNotFound', { id: userId }, 404);
    }

    const unliked = await UserLikeModel.findOneAndUpdate(
      { user: userId },
      {
        $pull: { likedBy: userLikes._id },
      },
      {
        new: true,
      }
    );

    return unliked.toJSON();
  };

  mostLikedUsers = async (): Promise<UserLike[]> => {
    const mostLikedUsers = await UserLikeModel.aggregate([
      {
        $addFields: {
          numberOfLikes: {
            $size: '$likedBy',
          },
        },
      },
      {
        $sort: { numberOfLikes: -1 },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $unwind: '$user',
      },
      { $unset: 'user.password' },
    ]);

    return mostLikedUsers;
  };
}

const userLikeService = new UserLikeService();

export default userLikeService;
