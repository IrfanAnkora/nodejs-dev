import { Response, Router } from 'express';
import { generateFailure, generateResponse } from '../../core/response';
import { authUser } from '../../core/auth.middleware';
import { RequestType } from '../../core/request.type';
import authService from './auth.service';

class AuthController {
  constructor(router: Router) {
    router.route('/signup').post(this.signUp);
    router.route('/login').post(this.login);
    router.route('/me').get(authUser, this.getMe);
    router.route('/me/update-password').put(authUser, this.updatePassword);
  }

  signUp = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await authService.signUp(req.body));
    } catch (e) {
      return generateFailure(res, e);
    }
  };

  login = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await authService.login(req.body));
    } catch (e) {
      return generateFailure(res, e);
    }
  };

  getMe = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await authService.getMe(req.user));
    } catch (e) {
      return generateFailure(res, e);
    }
  };

  updatePassword = async (req: RequestType, res: Response) => {
    try {
      return generateResponse(res, await authService.updatePassword(req.user, req.body));
    } catch (e) {
      return generateFailure(res, e);
    }
  };
}

export default AuthController;
