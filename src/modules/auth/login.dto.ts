import { throwError } from '../../core/response';

export class LoginDto {
  username: string;
  password: string;
  constructor(data: LoginDto) {
    if (!data.username || !data.password) {
      throwError('InvalidData', null, 400);
    }
    Object.assign(this, data);
  }
}
