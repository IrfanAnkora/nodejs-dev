export class LoginResponseDto {
  token: string;
  constructor(data: LoginResponseDto) {
    Object.assign(this, data);
  }
}
