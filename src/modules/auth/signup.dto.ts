import { throwError } from '../../core/response';

export class SignupDto {
  username: string;
  password: string;
  constructor(data: SignupDto) {
    if (!data.username || !data.password) {
      throwError('InvalidData', null, 400);
    }
    Object.assign(this, data);
  }
}
