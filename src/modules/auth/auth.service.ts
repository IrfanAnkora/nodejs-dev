import UserModel, { User } from '../user/user.entity';
import { SignupDto } from './signup.dto';
import bcrypt from 'bcrypt';
import { UserDto } from '../user/user.dto';
import jwt from 'jsonwebtoken';
import { LoginDto } from './login.dto';
import { LoginResponseDto } from './loginResponse.dto';
import { TokenData } from '../../core/auth.middleware';
import { CONFIG } from '../../core/config';
import { UpdatePasswordDto } from './updatePassword.dto';
import UserLikeModel from '../userLike/userLike.entity';
import { throwError } from '../../core/response';

const saltRounds = 10;

class AuthService {
  signUp = async (data: SignupDto): Promise<UserDto> => {
    const signupData = new SignupDto(data);

    const userExists = await UserModel.findOne({
      username: signupData.username,
    });
    if (userExists) {
      throwError('UserExists', null);
    }

    const hashedPassword = await bcrypt.hash(signupData.password, saltRounds);
    signupData.password = hashedPassword;

    const user = (await UserModel.create(signupData)).toJSON();
    await UserLikeModel.create({
      user: user._id,
      likedBy: [],
    });

    return new UserDto(user);
  };

  login = async (data: LoginDto): Promise<LoginResponseDto> => {
    const loginData = new LoginDto(data);

    const user = await UserModel.findOne({
      username: loginData.username,
    });
    if (!user) {
      throwError('UserNotExists', null);
    }

    const isValid = await bcrypt.compare(loginData.password, user.password);
    if (!isValid) {
      throwError('InvalidPassword', null);
    }

    const tokenData = new TokenData(user);
    const token = jwt.sign({ ...tokenData }, CONFIG.JWT_SECRET);

    return new LoginResponseDto({ token });
  };

  getMe = async (user: User): Promise<UserDto> => {
    return new UserDto(user);
  };

  updatePassword = async (user: User, data: UpdatePasswordDto): Promise<UserDto> => {
    const passwordData = new UpdatePasswordDto(data);

    const isValid = await bcrypt.compare(passwordData.oldPassword, user.password);
    if (!isValid) {
      throwError('InvalidPassword', null);
    }
    const hashedPassword = await bcrypt.hash(passwordData.newPassword, saltRounds);
    const newPassword = hashedPassword;

    const updatedUser = await UserModel.findOneAndUpdate(
      { _id: user._id },
      {
        password: newPassword,
      },
      {
        new: true,
      }
    );

    return new UserDto(updatedUser.toJSON());
  };
}

const authService = new AuthService();

export default authService;
