import { throwError } from '../../core/response';

export class UpdatePasswordDto {
  oldPassword: string;
  newPassword: string;
  constructor(data: UpdatePasswordDto) {
    if (!data.oldPassword || !data.newPassword) {
      throwError('InvalidData', null, 400);
    }
    Object.assign(this, data);
  }
}
