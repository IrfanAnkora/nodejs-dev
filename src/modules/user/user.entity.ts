import { prop, getModelForClass } from '@typegoose/typegoose';

export class User {
  public _id?: string;
  @prop({ type: String })
  public username?: string;

  @prop({ type: String })
  public password?: string;

  constructor(obj?: User) {
    Object.assign(this, obj);
  }
}

const UserModel = getModelForClass(User);

export default UserModel;
