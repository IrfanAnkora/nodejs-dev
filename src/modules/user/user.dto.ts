import { User } from './user.entity';

export class UserDto extends User {
  constructor(user: User) {
    super(user);
    delete this.password;
  }
}
