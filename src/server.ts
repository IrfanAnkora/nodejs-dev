import express, { Router } from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import { CONFIG } from './core/config';
import UserLikeController from './modules/userLike/userLike.controller';
import AuthController from './modules/auth/auth.controller';

const initialize = async () => {
  try {
    await mongoose.connect(CONFIG.DB_URL);

    console.info('Successfully connected to db');

    const app = express();
    app.use(bodyParser.json());

    const router = Router();

    new UserLikeController(router);
    new AuthController(router);

    app.use(router);

    if (process.env.NODE_ENV !== 'test') {
      app.listen(CONFIG.PORT);
      console.info('App running on port:', CONFIG.PORT);
    }
    return app;
  } catch (e) {
    console.error('Problem with running server:', e);
  }
};

export default initialize();
