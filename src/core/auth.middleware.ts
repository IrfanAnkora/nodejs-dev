import { Response, NextFunction } from 'express';
import UserModel from '../modules/user/user.entity';
import jwt from 'jsonwebtoken';
import { UserDto } from '../modules/user/user.dto';
import { CONFIG } from './config';
import { RequestType } from './request.type';
import { generateFailure, throwError } from './response';

export class TokenData {
  userId: string;
  constructor(user: UserDto) {
    this.userId = user._id;
  }
}

export const authUser = async (req: RequestType, res: Response, next: NextFunction) => {
  try {
    const { authorization } = req.headers;
    if (!authorization) {
      throwError('Unauthorized', { message: 'No auth found' }, 401);
    }

    if (!authorization.startsWith('Bearer ')) {
      throwError('Unauthorized', { message: 'No bearer found' }, 401);
    }

    const token = authorization.split('Bearer ')[1];
    if (!token) {
      throwError('Unauthorized', { message: 'No token found' }, 401);
    }

    let decoded: TokenData = null;
    try {
      decoded = jwt.verify(token, CONFIG.JWT_SECRET) as TokenData;
    } catch (e) {
      throwError('Unauthorized', { message: 'Invalid token' }, 401);
    }
    const user = await UserModel.findOne({ _id: decoded.userId });

    if (!user) {
      throwError('UserNotExists', { userId: decoded.userId }, 401);
    }

    req.user = user.toJSON();
    next();
  } catch (e) {
    return generateFailure(res, e);
  }
};
