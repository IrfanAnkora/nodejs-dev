import dotenv from 'dotenv';

dotenv.config({ path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env' });

export const CONFIG = {
  PORT: process.env.PORT || 4000,
  JWT_SECRET: 'nodejsdevelopment',
  DB_URL: process.env.MONGO_URL,
};
