import { Request } from 'express';
import { User } from '../modules/user/user.entity';

export type RequestType = Request & { user: User };
