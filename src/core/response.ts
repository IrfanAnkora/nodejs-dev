import { Response } from 'express';

export const generateResponse = (res: Response, data: any, metadata?: any) => {
  const responseData = {
    data,
    metadata,
  };
  if (!metadata) {
    delete responseData.metadata;
  }
  return res.send(responseData);
};

interface ErrorType {
  name: string;
  data: any;
  statusCode: number;
}

export const generateFailure = (res: Response, error: Error | ErrorType) => {
  let { name, data, statusCode } = error as ErrorType;
  if (!statusCode) {
    statusCode = 500;
    name = 'InternalError';
    data = { message: (error as Error).message };
  }
  return res.status(statusCode).send({
    statusCode,
    name,
    data,
  });
};

export const throwError = (name: string, data: any, statusCode: number = 400) => {
  throw {
    name,
    data,
    statusCode,
  };
};
