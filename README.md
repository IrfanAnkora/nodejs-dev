# NodeJS Development assignemnt

## Notes

Usually, the `.env` files would be omitted from source control, but in this case, I'm including them since there is no sensitive data in this project and it will make running and testing the app much easier. I realise that exposing this data in source control is a security risk in production applications, but for the sake of the test developing assignment, I'm keeping them for the ease of the reviewer.

## Details

### Database

This project is using MongoDB with `typegoose` and `mongoose` ORM. To run the project, you will need either a local instance of MongoDB or if you want to check out the production variant, you can run it with Docker.

To ease testing, there are included 2 databases from mLab, which can be used to avoid running a local instance of MongoDB.

### Docker

The `Dockerfile` and `docker-compose.yaml` files are set up to build a "production" variant of the app. Dockerfile is used to create a production build by copying the necessary files, running a build, cleaning up and exposing the app on port `4000`.

To run the docker instance, you will need Docker installed on your local machine. To run the instance, all that's needed is a simple `docker-compose up`.

If you make changes in the docker config or the build process in Docker, the easiest way to get the latest changes is to force a rebuild and recreation `docker-compose up --force-recreate --build`.

## Requirements

- NodeJS v10
